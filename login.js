var loginState = {
    selection: 1,
    preload: function () {
        // Add a 'loading...' label on the screen 
        loadingLabel = game.add.text(game.width / 2, 80, 'Please Log In ', {
            font: '30px Arial',
            fill: '#ffffff'
        });
        loadingLabel.anchor.setTo(0.5, 0.5);

        googleLabel = game.add.text(game.width / 2, 150, 'Google', {
            font: '30px Arial',
            fill: '#ffffff'
        });
        googleLabel.anchor.setTo(0.5, 0.5);

        anonymousLabel = game.add.text(game.width / 2, 200, 'Anonymous', {
            font: '30px Arial',
            fill: '#ffffff'
        });
        anonymousLabel.anchor.setTo(0.5, 0.5);

        upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        upKey.onDown.add(this.changeSelection, this);
        downKey.onDown.add(this.changeSelection, this);
        enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.enterReaction, this);


    },
    update: function () {
        if (this.selection == 1) {
            anonymousLabel.setText('Anonymous');
            googleLabel.setText('*Google*');
            googleLabel.fill = '#ff3300';
            anonymousLabel.fill = '#ffffff';
        } else {
            anonymousLabel.setText('*Anonymous*');
            googleLabel.setText('Google');
            googleLabel.fill = '#ffffff';
            anonymousLabel.fill = '#ff3300';
        }
    },
    changeSelection: function () {
        if (this.selection == 1) this.selection = 2;
        else this.selection = 1;
    },
    enterReaction: function () {
        if (this.selection == 1) this.googleLogin();
        else this.Anonymousplayer();
    },
    googleLogin: function () {
        initApp();
    },
    Anonymousplayer: function () {
        game.state.start('menu');
    },

    create: function () { // Go to the menu state 
        //game.state.start('menu');
    }
};


function initApp() {
    // Login with Email/Password

    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider)
        .then(function () {
            game.state.start('menu');
        })
        .catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            //alert('something wrong');
            ///create_alert("error", errorMessage);
            //txtEmail.value = "";
            //txtPassword.value = "";
        });
}