var lastTime = 0;
var playState = {

    preload: function () {},
    // The proload state does nothing now. 

    create: function () {
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'space': Phaser.Keyboard.SPACEBAR
        });
        window.onkeydown = function () {
            if (keyboard.space.isDown && status == 'goingDown') {
                game.paused = !game.paused;
                if (game.paused == true) {
                    text5.visible = true;
                } else {
                    text5.visible = false;
                }
            }
        };
        Ahh = game.add.audio('Ahh');
        this.createBounders();
        this.createplayer();
        this.createTextsBoard();
    },

    update: function () {
        // bad
        if (status == 'Over' && keyboard.enter.isDown) {
            status = 'goingDown';
            this.restart();
            //game.state.start('saving');
            //playState.preload();
        }
        if (status != 'goingDown') return;

        this.physics.arcade.collide(player1, platforms, this.platformReaction);
        this.physics.arcade.collide(player1, [leftWall, this.rightWall]);
        this.checkTouchCeiling(player1);

        if (multiPlayer == true) {
            this.physics.arcade.collide(player2, platforms, this.platformReaction);
            this.physics.arcade.collide(player2, [leftWall, this.rightWall]);
            this.checkTouchCeiling(player2);
        }

        this.physics.arcade.collide(player1, player2, this.playerPush);




        this.updatePlatforms();
        this.updateTextsBoard();
        this.createPlatforms();

        this.movePlayer();
        this.playerDie();

    },


    movePlayer: function () {
        if (keyboard.left.isDown && player1.body.x > 17) {
            player1.body.velocity.x = -250;
        } else if (keyboard.right.isDown && player1.body.x < 350) {
            player1.body.velocity.x = 250;
        } else {
            player1.body.velocity.x = 0;
        }
        this.setplayerAnimate(player1);

        if (multiPlayer == true) {
            if (keyboard.a.isDown && player2.body.x > 17) {
                player2.body.velocity.x = -250;
            } else if (keyboard.d.isDown && player2.body.x < 350) {
                player2.body.velocity.x = 250;
            } else {
                player2.body.velocity.x = 0;
            }
            this.setplayerAnimate(player2);
        }

    },

    playerDie: function () {
        // When the player dies, go to the menu 
        if (multiPlayer == false) {
            if (player1.life <= 0 || player1.body.y > 430) {
                score = distance;
                init(multiPlayer, distance, Level);
                Ahh.play();
                this.gameOver();
            }
        } else {
            if (player1.life <= 0 || player1.body.y > 430) {
                Ahh.play();
                player1.visible = false;
            }
            if (player2.life <= 0 || player2.body.y > 430) {
                Ahh.play();
                player2.visible = false;
            }
            if ((player1.life <= 0 || player1.body.y > 430) && (player2.life <= 0 || player2.body.y > 430)) {
                this.gameOver();
                score = distance;
                init(multiPlayer, distance, Level);
            }
        }
    },

    createPlatforms: function () {
        if (game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            distance += 1;
        }
    },
    createBounders: function () {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(383, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        ceiling = game.add.image(0, 0, 'ceiling');
    },
    createOnePlatform: function () {

        var platform;
        var x = Math.random() * (400 - 96 - 40) + 20;
        var y = 400;
        var rand = Math.random() * 100;


        if (rand % (Level + 10) < 6) {
            platform = game.add.sprite(x, y, 'basic');
        } else if (rand % (Level + 10) < 7) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand % (Level + 10) < 8) {
            platform = game.add.sprite(x, y, 'rollLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand % (Level + 10) < 9) {
            platform = game.add.sprite(x, y, 'rollRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand % (Level + 10) < 10) {
            platform = game.add.sprite(x, y, 'bungyJumping');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'rollOver');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    createplayer: function () {

        player1 = game.add.sprite(200, 50, 'player1');
        player1.direction = 10;
        game.physics.arcade.enable(player1);
        player1.body.gravity.y = 500;
        player1.animations.add('left', [0, 1, 2, 3], 8);
        player1.animations.add('right', [9, 10, 11, 12], 8);
        player1.animations.add('flyleft', [18, 19, 20, 21], 12);
        player1.animations.add('flyright', [27, 28, 29, 30], 12);
        player1.animations.add('fly', [36, 37, 38, 39], 12);
        player1.life = 10;
        player1.unbeatableTime = 0;

        if (multiPlayer == true) {
            player2 = game.add.sprite(168, 50, 'player2');
            player2.direction = 10;
            game.physics.arcade.enable(player2);
            player2.body.gravity.y = 500;
            player2.animations.add('left', [0, 1, 2, 3], 8);
            player2.animations.add('right', [9, 10, 11, 12], 8);
            player2.animations.add('flyleft', [18, 19, 20, 21], 12);
            player2.animations.add('flyright', [27, 28, 29, 30], 12);
            player2.animations.add('fly', [36, 37, 38, 39], 12);
            player2.life = 10;
            player2.unbeatableTime = 0;
        }
    },

    createTextsBoard: function () {
        var style = {
            fill: '#ff0000',
            fontSize: '20px'
        }
        text1 = game.add.text(10, 10, '', style);
        text2 = game.add.text(180, 10, '', style);
        text3 = game.add.text(300, 10, '', style);
        text4 = game.add.text(100, 200, 'Press Enter To Restart', style);
        text4.visible = false;
        text5 = game.add.text(150, 200, 'PAUSED', style);
        text5.visible = false;
    },

    setplayerAnimate: function (player) {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;

        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
            player.frame = 8;
        }
    },

    updatePlatforms: function () {
        for (var i = 0; i < platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 2;
            if (platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },

    updateTextsBoard: function () {
        text1.setText('P1 life:' + player1.life);
        text2.setText('B' + distance);

        if (multiPlayer == true) {
            text3.setText('P2 life:' + player2.life);
        }
    },

    platformReaction: function (player, platform) {
        if (platform.key == 'rollRight') {
            player.body.x += 2;
        }
        if (platform.key == 'rollLeft') {
            player.body.x -= 2;
        }
        if (platform.key == 'bungyJumping') {
            platform.animations.play('jump');
            player.body.velocity.y = -350;
        }
        if (platform.key == 'nails') {
            if (player.touchOn !== platform) {
                player.life -= 3;
                player.touchOn = platform;
                game.camera.flash(0xff0000, 100);
            }
        }
        if (platform.key == 'basic') {
            if (player.touchOn !== platform) {
                if (player.life < 10) {
                    player.life += 1;
                }
                player.touchOn = platform;
            }
        }
        if (platform.key == 'rollOver') {
            if (player.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function () {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }

    },

    playerPush: function (player1, player2) {
        player1.body.velocity.x = (player1.body.velocity.x + player2.body.velocity.x) / 2 * 0.5;
        player1.body.velocity.x = (player1.body.velocity.x + player2.body.velocity.x) / 2 * 0.5;
    },

    checkTouchCeiling: function (player) {
        if (player.body.y < 0) {
            if (player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if (game.time.now > player.unbeatableTime) {
                player.life -= 3;
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
            }
        }
    },

    gameOver: function () {
        text4.visible = true;
        platforms.forEach(function (s) {
            s.destroy()
        });
        platforms = [];
        status = 'Over';
    },

    restart: function () {
        text4.visible = false;
        distance = 0;
        this.createplayer();
        this.createOnePlatform;
        status = 'goingDown';
    }

};

function init(multi, score, level) {
    var newpostref = firebase.database().ref('score_list').push();

    newpostref.set({
        Multi: multi,
        Score: score,
        Level: level,
        Time: Date()
    });

}